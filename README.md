# Übungsaufgaben zum Programmierenlernen in TeX und LaTeX

- [main.pdf](main.pdf) enthält die Übungsaufgaben mit Hilfestellungen
- [solution/](solution) enthält die Musterlösungen
- [test/](test) enthält Beispieldokumente um die Lösungen zu testen
- [content/](content) enthält den LaTeX Source Code für die Übungsaufgaben und Hilfestellungen
- [preamble/](preamble) enthält die Präambel für die Übungsaufgaben und Hilfestellungen
- [lexer/](lexer) enthält einen leicht abgewandelten [Pygments](https://pygments.org/) [Lexer](https://de.wikipedia.org/wiki/Tokenizer), damit [minted](https://www.ctan.org/pkg/minted) versteht, dass @ Teil eines Makronamens sein kann


Grundkenntnisse über die Funktionsweise von TeX werden vorausgesetzt.

- [The TeXbook](https://www-cs-faculty.stanford.edu/~knuth/abcde.html#texbk) von Donald E. Knuth, dem Entwickler von TeX, ist ein hervorragendes Lehrbuch um einen einfachen Einstieg in TeX zu bekommen.
- [TeX by Topic](https://www.ctan.org/pkg/texbytopic) von Victor Eijkhout ist ein hervorragendes, freies Nachschlagewerk. Für Leute, die sich nicht davor scheuen sofort mit der ganzen Komplexität konfrontiert zu werden, mag auch dies ein guter Einstieg in TeX sein.


Die Musterlösungen verwenden (wo sie LaTeX und nicht nur Plain TeX verwenden) noch die alten LaTeX2e Makros und noch nicht die neue LaTeX3 Programming Language expl3.
Auch wenn LaTeX3 noch nicht fertig ist, sind die stabilen Teile von expl3 bereits im LaTe2e-Kernel enthalten.

- [macros2e](https://www.ctan.org/pkg/macros2e) und [source2e](https://www.ctan.org/pkg/source2e) beschreiben die LaTeX2e Kommandos
- [usrguide](https://www.ctan.org/pkg/usrguide) beschreibt die neuen LaTeX3 Programmier-Kommandos für Dokumenten-Autoren
- [interface3](https://www.ctan.org/pkg/l3kernel) beschreibt die neue LaTeX3 Programmier-Schnittstelle für Paket-Entwickler
