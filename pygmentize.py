#!/usr/bin/env python3

import os
import sys
import subprocess

latex_lexer = os.path.join(os.path.dirname(__file__), 'lexer', 'latex_atletter.py:TexLexer')
cmd = ['pygmentize'] + sys.argv[1:]
if 'latex' in sys.argv:
	if '-l' in sys.argv:
		i = sys.argv.index('-l')
		cmd[i+1] = latex_lexer
		cmd.insert(i+2, '-x')
	#else:
	#	cmd.insert(1, '-l')
	#	cmd.insert(2, latex_lexer)
#print(f'! running {cmd}', file=sys.stderr)
subprocess.run(cmd)
