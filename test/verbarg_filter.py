#!/usr/bin/env python

import argparse
import os
import re


def parse_commandline_arguments():
	def regex(flags=0):
		def regex(pattern):
			try:
				return re.compile(pattern, flags)
			except re.error as e:
				raise ValueError(e)

		return regex

	METAVAR_RE = "REGEX"

	parser = argparse.ArgumentParser()
	parser.add_argument("-f", "--filename", metavar=METAVAR_RE, action="append", dest="reos_filenames", type=regex(re.IGNORECASE))
	parser.add_argument("-c", "--content" , metavar=METAVAR_RE, action="append", dest="reos_lines", type=regex(re.IGNORECASE))
	parser.add_argument("-o", "--out" , action="store", dest="ffn_out", metavar="FILENAME", default=os.path.splitext(os.path.split(__file__)[1])[0]+"_results.tex")
	parser.add_argument("-p", "--path", action="store", dest="path", default="../solution")

	return parser.parse_args()


def main(path, reos_filenames, reos_lines, ffn_out):
	args = parse_commandline_arguments()

	with open(ffn_out, 'wt') as f:
		f.write(r"\begin{itemize}")
		f.write("\n")

		found = False

		for ffn_in in iter_filenames(path, reos_filenames):
			if reos_lines:
				lines = list(iter_lines(ffn_in, reos_lines))
			else:
				lines = None

			if lines or not reos_lines:
				fn = os.path.split(ffn_in)[1]
				f.write(r"\item \filename{%s}" % fn)
				f.write("\n\n")
				found = True

				if lines:
					f.write(r"\begin{tabular}{@{} rl @{}}")
					f.write("\n")
					for ln_number, ln in lines:
						f.write(r"%4s & %s \\" % (ln_number, mint(ln)))
						f.write("\n")
					f.write(r"\end{tabular}")
					f.write("\n\n")

		if not found:
			f.write(r"\item (no results)")

		f.write(r"\end{itemize}")
		f.write("\n")


def iter_filenames(path, reos, ext=".tex"):
	for fn in os.listdir(path):
		ffn = os.path.join(path, fn)
		if is_interesting_file(ffn, fn, reos, ext):
			yield ffn

def is_interesting_file(ffn, fn, reos, ext):
	'''ffn: Full File Name, fn: File Name (without path), reos: iterable of compiled Regular Expression ObjectS, ext: wanted file EXTension'''

	if not os.path.isfile(ffn):
		return False

	if fn[-len(ext):] != ext:
		return False

	if not reos:
		return True

	for reo in reos:
		m = reo.search(fn)
		if m:
			return True

	return False


def iter_lines(ffn, reos):
	'''ffn: Full File Name, reos: iterable of compiled Regular Expression ObjectS'''

	with open(ffn, 'rt') as f:
		ln_number = 0
		for ln in f:
			ln = ln.rstrip("\n")
			ln_number += 1
			for reo in reos:
				if reo.search(ln):
					yield ln_number, ln


def mint(code):
	delimiters = "|!#~+*=$\"'§?/.,-_@`^°0123456789"
	for d in delimiters:
		if d not in code:
			return r"\latexinline{delimiter}{code}{delimiter}".format(delimiter=d, code=code)

	raise ValueError("failed to find a delimiter for %r" % code)


if __name__ == '__main__':
	args = parse_commandline_arguments()
	main(args.path, args.reos_filenames, args.reos_lines, args.ffn_out)
